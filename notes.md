1.Вам потрібно додати нове домашнє завдання в новий репозиторій на Gitlab.

Створюємо новий репозиторій GitLab, клонуємо репозиторій на свій комп'ютер за допомогою команди git clone.
Вибираємо необхідну папку cd test
Додаємо зміни git add .
Коммітимо зміни git commit -m "Commit"
Відправляємо в репозиторій git push --set-upstream origin main

2.Вам потрібно додати існуюче домашнє завдання в новий репозиторій на Gitlab.

Створюємо новий репозиторій GitLab
Обираємо існуючу папку cd existing folder
Ініціюємо гіт git init --initial-branch=main
Додаємо віддалений репозиторій git remote add origin
Додаємо зміни git add .
Коммітимо зміни git commit -m "Commit"
Відправляємо завдання у репозиторій git push --set-upstream origin main